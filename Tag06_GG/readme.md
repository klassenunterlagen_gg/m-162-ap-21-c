# Datenstruktur Tabelle
# 1. Fragen über drei/fünf Tabellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Einsatz von Verknüpfungen zwischen Tabellen erkennen. |

Sie finden im Folgenden Fragedokumente und Tabellen. Gehen Sie gemäss der Anleitung vor. 

## 1.1. Vorgehen
 1. Im Dokument [Einstiegsfragen.pdf](Arbeitsdateien/Einstiegsfragen_1.pdf) finden Sie Fragen, die Sie mit den [drei Tabellen](Arbeitsdateien/Tabelleneinstieg.pdf) beantworten können.
 2. Speichern Sie die Ergebnisse für sich ab.
 3. Bearbeiten Sie das Dokument[Einstiegsfragen 2](Arbeitsdateien/Einstiegsfragen_2.pdf)
 4. Erstellen Sie eine grafische Übersicht mit den 5 Tabellen. Ob Papier oder digital ist Ihre Entscheidung.
 5. Dann beantworten Sie die Fragen mit den [Zusatztabellen](Arbeitsdateien/Zusatztabellen.pdf)

## 1.2. Lösungen
Für die ersten Fragen habe ich die Lösungen nicht elektronisch - ist zu simpel :-) - glaube ich. Habe es doch noch gemacht: [Lösungen 1. Blatt](Arbeitsdateien/Einstiegsfragen_1_Lösungen.pdf) <br>
Für das 2. Blatt finden Sie hier die Lösungen: [Auftrag 2](Arbeitsdateien/Einstiegsfragen_2_Lösungen.pdf)

## 2. Tools
Um solche Übersichten von Tabellen zukünftig einfacher darzustellen, werden wir ein Tool namens "MySQL-Workbench" verwenden.
Den Download finden Sie hier: [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) Sie müssen sich nicht registrieren und auch keine Mailadresse angeben! Zur Workbench gibt es im Internet viele Anleitungen und Erklärungen. Trotzdem werde ich im 2. Teamsmeeting einen kurzen Einblick geben.

## 3. weiter geht's
Vorerst verwenden wir dieses provisorische Skript: [Modul 162](Arbeitsdateien/SkriptMod_162_V1.pdf). 
