# Auftrag 01

Erstellen Sie ein konzeptuelles Datenmodell nach chen-Notation.

Das Datenmodell beinhaltet eine Tabelle "Personen".

Folgende Attribute sind notwendig:

- PersonenNr
- Vorname
- Nachname
- Geburtsdatum
- Adresse
- Ort

Hilfe bietet Ihnen das Dokument [Das konzeptionelle Datenmodell](./ERD/Das_konzeptionelle_Datenmodell.pdf)

**Zeit**: 30 Minuten

**Form**: in 2er Gruppen

**Tools**: draw.io

