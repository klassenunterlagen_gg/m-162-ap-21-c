# Auftrag 02

Erweitern Sie das Datenmodell aus Auftrag 01 mit der Tabelle "Rechnungen" und einer Beziehung zueinander.

Folgende Attribute sind notwendig:

- Rechnungsnummer
- Rechnungsdatum



*Frage*

- Welcher Beziehungstyp macht am meisten Sinn?



**Zeit**: 15 Minuten

**Form**: in 2er Gruppen

**Tools**: draw.io

