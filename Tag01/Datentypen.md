# Einfache Datentypen

[TOC]

**Autoren**: Andy Corsten
**Datum**: 16.08.2021

## Ganze Zahlen

**Bezeichnung**: BIGINT, BIN, BIN FIXED, BINARY, BYTE, COMP, INT, INTEGER, LONG, LONG INT, LONGINT, MEDIUMINT, SHORT, SHORTINT, SMALLINT

**Wertebereich**: Meist 32 Bit (−231…231-1), 16 Bit, 64 Bit

**Operationen**: +, -, *, <, >, =, Division mit Rest und Modulo

## Natürliche Zahlen

**Bezeichnung**: BYTE, CARDINAL, NATURAL, UNSIGNED, UNSIGNED CHAR, UNSIGNED INT, UNSIGNED LONG, UNSIGNED SHORT, WORD

**Wertebereich**: Meist 32 Bit, (0…232-1), 8 Bit, 16 Bit, 64 Bit

**Operationen**: +, -, *, <, >, =, Division mit Rest und Modulo

## Festkommazahlen (Dezimalzahlen)

**Bezeichnung**: COMP-3, CURRENCY, PACKED DECIMAL, DEC, DECIMAL, NUMERIC

**Wertebereich**: Wertebereich direkt abhängig von der maximalen Stellenanzahl, die meist vorzugeben ist; CURRENCY (64 Bit): -922337203685477,5808…922337203685477,5807

**Operationen**: +, -, *, <, >, =, Division mit Rest und Modulo

## Aufzählungstypen

**Bezeichnung**: ENUM, SET oder implizit

**Wertebereich**: Frei wählbar, beispielsweise (SCHWARZ, ROT, BLAU, GELB)

**Operationen**: <, >, =

## Boolean (logische Werte)

**Bezeichnung**: BOOL, BOOLEAN, LOGICAL, oder (implizit ohne Bezeichner)

**Wertebereich**: (TRUE, FALSE) oder (= 0, ≠ 0) oder (= -1, = 0)

**Operationen**: NOT, AND, XOR, NOR, NAND, OR, =, ≠

## Zeichen (einzelnes Zeichen)

**Bezeichnung**: CHAR, CHARACTER

**Wertebereich**: Alle Elemente des Zeichensatzes (zum Beispiel Buchstaben)

**Operationen**: <, >, =, Konvertierung in INTEGER, …

## Gleitkommazahlen

**Bezeichnung**: DOUBLE, DOUBLE PRECISION, EXTENDED, FLOAT, HALF, LONGREAL, REAL, SINGLE, SHORTREAL

**Wertebereich**: Verschiedene Definitionen (siehe unten)

**Operationen**: +, -, *, /, <, >, =

## Zeiger

**Bezeichnung**: ACCESS, POINTER, IntPtr oder auch nur kurz Stern (*)

**Wertebereich**: Adresse des Basistyps (oft anonym)

**Operationen**: Referenz, Dereferenz, in einigen Sprachen: +, -, *, /

## Konstanter Nullzeiger

**Bezeichnung**: NULL, VOID, None, NIL, Nothing

**Wertebereich**: keiner

**Operationen**: =

**Bedeutung**: Dieser Zeiger ist verschieden von allen Zeigern auf Objekte.

## Zeichenkette fester Länge (Zusammengesetzt)

(Im Grunde sind Zeichenketten selbst nur eine Reihung des Typs Character (Zeichen). Da sie jedoch in vielen Programmiersprachen vordefiniert sind, werden sie hier gesondert aufgelistet.) 

**Bezeichnung**: Array of CHAR, CHAR(n), CHAR[n]

**Wertebereich**: Alle möglichen Zeichenketten

**Operationen**: Stringfunktionen (Teilstring, Konkatenation [Zusammensetzung]), <, >, =

## Zeichenkette variabler Länge (Zusammengesetzt)

Die Länge ist feststellbar, implizit durch ein Metazeichen als String-Endezeichen (ASCII \0), explizit durch eine Variable, oder über eine Standardfunktion. Häufig als Abstrakter Datentyp in einer Standardbibliothek. 

**Bezeichnung**: String, Array of CHAR, VARCHAR, CLOB, Text

**Wertebereich**: Zeichenketten variabler Länge

**Operationen**: Stringfunktionen (Teilstring, Länge, Konkatenation [Zusammensetzung]), <, >, =
