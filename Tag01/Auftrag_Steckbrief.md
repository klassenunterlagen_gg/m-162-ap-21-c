# Auftrag: Steckbrief

Verfassen Sie einen Steckbrief über Ihre Person. Machen Sie darin Aussagen über Ihre
Interessen an 

- Demographische Angaben (Alter, Geschlect, Name, etc)
- Computern/Informatik
- Ihre Privatinteressen (Musik, Videospiele, Sport, etc). 

Die Steckbriefe werden Ihre KollegenInnen später lesen können. 

#### Zeit und Form

15 Minuten

**Handschriftlich** auf A4 Blatt

