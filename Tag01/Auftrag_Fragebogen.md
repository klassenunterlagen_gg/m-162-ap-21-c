# Auftrag: Fragebogen

Erstellen sie einen Fragebogen zu einem beliebigen Thema.  Folgende Datentypen und Merkmalstypen sollen vorkommen:

- Stetige Daten (min. 2 Fragen)
- Diskrete Daten (min. 3 Fragen)
  - Ordinalskala (min. 1 Frage)
  - Nominalskala (min. 1 Frage)

Sie werden ihre Daten später auswerten.

**Zeit**: 20 Minuten

**Form**: in 2er Gruppen

**Tools**: Google-Forms, Microsoft-Forms, Andere

