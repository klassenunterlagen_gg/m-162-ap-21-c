# Auftrag: Datentypen

#### Aufgabe 1

Welche Datentypen könnten den folgenden Werten zugrunde liegen?

| Wert                                           | Datentyp |
| ---------------------------------------------- | -------- |
| A                                              |          |
| 23                                             |          |
| 12021.25                                       |          |
| 0                                              |          |
| Ciara                                          |          |
| 12.2341235211                                  |          |
| true                                           |          |
| oben,unten                                     |          |
| [12,53,23,22,55]                               |          |
| .f},ÌúþÃ¸ìîìbõ©/=¹ryïòƒÇâ‘hðÂ.š¿ã‘q            |          |
| {name: "Meier", vorname: "Maxwell", alter: 21} |          |

#### Aufgabe 2

Finden sie die entsprechenden Gegenwerte

- Das Zeichen C in ganzer Zahl ausgedrückt
- Das Zeichen c in ganzer Zahl ausgedrückt
- Die ganze Zahl 32 als Zeichen ausgedrückt
- Die ganze Zahl 60 als Zeichen ausgedrückt
- Die ganze Zahl 111 als Zeichen ausgedrückt

#### Zeit und Form

- 30 Minuten
- Individuell

