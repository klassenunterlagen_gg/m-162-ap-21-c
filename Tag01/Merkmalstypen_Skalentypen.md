#### Merkmalstypen und Skalentypen

https://wissenschafts-thurm.de/grundlagen-der-statistik-worin-unterscheiden-sich-diskrete-und-stetige-merkmale-und-wann-sind-merkmale-haeufbar/

https://123mathe.de/merkmalsarten-und-merkmalsskalen

[Video Skalentypen](https://www.youtube.com/watch?v=RxN0vzAX6sU)

https://www.crashkurs-statistik.de/merkmals-und-skalentypen/



<img src="Skalenniveaus.jpg" alt="Skalenniveaus" style="zoom:80%;" />
Quelle: https://wissenschafts-thurm.de/grundlagen-der-statistik-worin-unterscheiden-sich-diskrete-und-stetige-merkmale-und-wann-sind-merkmale-haeufbar/
