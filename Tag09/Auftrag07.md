# Auftrag 07

Gegeben ist die Tabelle [Velo-Reparatur](./Veloreparatur.xlsx). 

- Analysieren Sie die Tabelle

- Gehen Sie schrittweise vor und Normalisieren Sie die Tabelle. Für jeden Normalfall erstellen Sie wiederum eine eigene Tabelle(n). 

- Erstellen Sie in mySQL-workbench die Tabellen und ein logisches Datenmodell in Krähenfuss-Notation.

  

**Zeit**: 90 Minuten

**Form**: in 2er Gruppen

**Tools**: Excel, mySQL-workbench

