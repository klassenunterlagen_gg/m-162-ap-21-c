# Statistik

https://novustat.com/statistik-blog/statistik-fuer-dummies-statistik-auswertung-mit-spss.html

Die Statistik unterteilt sich grob in die drei Bereiche deskriptive, explorative und induktive Statistik. Man beginnt seine Datenanalyse für gewöhnlich mit der deskriptiven (= beschreibenden) Statistik. In der deskriptiven Statistik geht es darum, mit Hilfe von Tabellen, Kennzahlen und Grafiken die Daten übersichtlich darzustellen. Im Anschluss kann man die explorative (= erkundende) oder induktive (= schließende) Statistik durchführen. Während die explorative Statistik Data-Mining Techniken und andere statistische Verfahren einsetzt, um Daten zu visualisieren und statistische Hypothesen zu erzeugen, fokussiert sich die induktive Statistik auf das Testen von Hypothesen und stützt sich dabei auf die Wahrscheinlichkeitstheorie.

## Begriffe der Statistik

### Messwerte, Ergebnis, Zufallsvariable

- Messwerte (measured values): gemessene, beobachtete oder abgelesene Werte. Es handelt sich
um die Quantität, welche erhoben wird. (Grösse in m)
- Ergebnis (result): Ergebnis einer Analyse nach der Durchführung der Messung und aller nachfolgender
Auswertungsschritte. (Durchschnittsgrösse)
- Zufallsvariable (variate): numerischer Wert eines Messwertes oder eines Ergebnisses. Merkmal,
dessen konkrete Ausprägungen sich von Untersuchungsobjekt zu Untersuchungsobjekt unterscheiden.
(XY: 1.82 m)
