# Auftrag: Corona Situation

Sie erstellen Diagramme zu der Corona-Situation vom 13.08.2021. Dazu öffnen sie die Excel-Datei [Auftrag_Diagramme_Quelle.xlsx](Auftrag_Diagramme_Quelle.xlsx) (Quelle: https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/situation-schweiz-und-international.html)

Hinweis: Alle relevanten Daten finden sie auf dem .

#### Aufgaben

- Erstellen sie ein Balken- oder Säulendiagramm, das die Verteilung der bestätigten Fälle aller Kantone gegenüberstellt. (Excel-Arbeitsblatt "Covid19 Kantone").
- Erstellen sie ein Balken- oder Säulendiagramm mit zwei Serien, die die Anzahl Fälle der Männer der Anzahl Fälle Frauen gegenüberstellt. (Excel-Arbeitsblatt "Covid19 Altersverteilung")
- Erstellen sie ein Liniendiagramm mit einer Serie, die den Verlauf der Fallzahlen pro Tag aufzeigt (Excel-Arbeitsblatt "Covid19 Zahlen")
- Stellen sie sicher, dass die Achsen korrekt beschriftet sind
- Stellen sie sicher, dass die Titel der Diagramme aussagekräftig sind.
- Stellen sie sicher, dass die Legenden vorhanden sind (falls notwendig).  
- Interpretieren sie die Daten in ein paar Sätzen. 
- Was wäre ihre Schlussfolgerung?

#### Zeit und Form

- 30 Minuten
- Abgabe als Excel-Daten
- Individuell
