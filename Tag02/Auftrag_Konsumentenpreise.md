# Auftrag: Konsumentenpreise

Erstellen sie eine Präsentation über die Konsumentenpreise. Sie finden die Daten und Informationen unter diesem Link: https://www.bfs.admin.ch/bfs/de/home/statistiken/preise/landesindex-konsumentenpreise.assetdetail.18324930.html

#### Aufgaben

- Laden sie die Daten herunter
- Verstehen sie was die Daten bedeuten. Sie können dies auch nachlesen 
- Erstellen sie die notwendigen Diagramme für die PowerPoint Präsentation
- Erstellen sie die PowerPoint Präsentation. 

#### Präsentation Inhalt

- Kurzer selbst verfasster Text, der die Daten erklärt, die Zugrunde liegen (Kein Copy-Paste von der Webseite)
- Diagramm 1: Zeigt die Entwicklung der Schweiz über die Monate. Diagrammtyp frei wählbar
- Diagramm 2: Zeigt den letzten Stand der Schweiz im Vergleich zu den anderen Ländern. Diagrammtyp frei wählbar
- Diagramm 3: Zeigt die Schweiz im Vergleich zu den anderen Ländern in diesem Jahr über die Monate. Diagrammtyp: Stapeldiagramm 

#### Zeit und Form

- 40 Minuten
- PowerPoint Präsentation
- 2er Gruppen

