# Chart Typen


| Chart-Typ                                                    | Daten-Typ / Merkmals-Typ                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Pie-Chart / Kuchendiagramm:<br /><img src="images/sc1_pie.png" alt="sc1_pie" style="zoom:60%;" /> | Diskret Ordinal<br />Diskret Nominal<br /><img src="images/sc1_diskret_ordinal.png" alt="sc1_diskret_ordinal" style="zoom:50%;" /><img src="images/dropdown.png" alt="sc1_diskret_ordinal" style="zoom:50%;" /><img src="images/mc_block.png" alt="mc_block" style="zoom:50%;" /> |
| Bar-Chart / Säulendiagramm:<br /><img src="images/sc1_bar.png" alt="sc1_bar" style="zoom:60%;" /> | Diskret Ordinal<br />Diskret Nominal<br />Halb-Stetig Metrisch: Nur wenn die Anzahl Optionen natürlich begrenzt sind <br /><img src="images/sc1_diskret_ordinal.png" alt="sc1_diskret_ordinal" style="zoom:50%;" /><img src="images/dropdown.png" alt="sc1_diskret_ordinal" style="zoom:50%;" /><img src="images/mc_block.png" alt="mc_block" style="zoom:50%;" /> |
| Bar-Chart / Säulendiagramm<br /><img src="images/sc2_bar.png" alt="sc2_bar" style="zoom:60%;" /> | Diskret Ordinal: Gegenüberstellungen<br /><img src="images/matrix-sc.png" alt="matrix_sc" style="zoom:50%;" /> |
| 100% Stacked Bar Chart / 100% Stapelbalkendiagramm<br /><img src="images/sc2_stack.png" alt="sc2_stack" style="zoom:60%;" /> | Diskret Ordinal: Gegenüberstellungen<br /><img src="images/matrix-sc.png" alt="matrix_sc" style="zoom:50%;" /> |
| Bar-Chart / Säulendiagramm<br /><img src="images/mc1_bar.png" alt="mc1_bar" style="zoom:60%;" /> | Diskret Nominal: Gegenüberstellungen<br /><img src="images/matrix-mc.png" alt="matrix_mc" style="zoom:50%;" /> |
| Stacked Bar Chart / Stapelbalkendiagramm<br /><img src="images/mc1_stack.png" alt="mc1_stack" style="zoom:60%;" /> | Diskret Nominal: Gegenüberstellungen<br /><img src="images/matrix-mc.png" alt="matrix_mc" style="zoom:50%;" /> |
| Line Chart / Liniendiagramm<br /><img src="images/zeitreihe.png" alt="zeitreihe" style="zoom:60%;" /> | stetig: Zeitreihen                                           |
| Kein Diagramm<br /><br />Antwort 1<br />Antwort 2<br />Antwort 3<br />etc | stetig: Kein Diagramm möglich. Liste von Antworten ausgeben  |





