# Diagramme erstellen

[TOC]



## Bezeichnungen

![bezeichnungen](images/bezeichnungen.png)



## Vorgehen Diagramm Erstellen

Folgend finden sie eine Anleitung wie immer zu einem korrekten Diagramm kommen. Es gibt auch Abkürzungen, z.B. wenn die Kategorien und die Serien gleich nebeneinanderliegen wie im vorliegenden Beispiel. Mit der hier beschriebenen Methode, können sie aber immer die richtigen Daten auswählen.

![Diagrammerstellen_1](images/Diagrammerstellen_1.png)

1. Markieren sie das Feld in Excel an dem sie das Diagramm einfügen möchten
2. Fügen sie unter dem Menupunkt "Insert" ein Diagramm hinzu ohne, dass sie vorher Daten markiert haben. Ein leeres Diagramm wird hinzugefügt.



![Diagrammerstellen_2](images/Diagrammerstellen_2.png)

3. Machen sie einen Rechtsklick auf die leere Diagrammfläche und wählen sie "Select Data..." aus. Es öffnet sich das folgende Fenster



![Diagrammerstellen_3](images/Diagrammerstellen_3.png)

4. Hier fügen sie die Serien/Datenreihen hinzu. Klicken sie hier drauf um eine Reihe hinzuzufügen. Es öffnet sich wieder ein neues Fenster
5. Auf diesen Punkt kommen wir später zurück



![Diagrammerstellen_4](images/Diagrammerstellen_4.png)

6. Sie haben im neuen Fenster zwei Felder. Das erste Feld ist für den Namen der Datenreihe. Klicken sie in das Feld und anschliessend auf den Bereich des Excels, welches die Bezeichnung der Reihe enthält. 
7. Klicken sie anschliessend in das zweite Feld und löschen den vor eingefüllten Inhalt. Anschliessend markieren sie in Excel alle Datenpunkte, die zu dieser Datenreihe/Serie gehören. Sie sehen im Bild bereits das Resultat. Sie können das Fenster bestätigen und so schliessen.



![Diagrammerstellen_5](images/Diagrammerstellen_5.png)

5. Nun sind sie zurück auf dem vorherigen Fenster und sie sehen, dass Rubriken / Kategorie automatisch hinzugefügt wurden. Es handelt sich hier aber nur um eine Nummerierung. Wir möchten nun die echten Rubriken hinzufügen. Klicken sie daher auf das Feld.



![Diagrammerstellen_6](images/Diagrammerstellen_6.png)

8. Auch hier öffnet sich wieder ein neues Fenster mit einem Feld. Löschen sie den Inhalt und markieren die Bezeichnungen, die zu der Datenreihe hinzugefügt wird. Bestätigen sie ihre Auswahl mit "OK" und so kehren sie zu dem Fenster zurück welches sie ebenfalls mit "OK" bestätigen. Sie haben nun die wichtigsten Elemente hinzugefügt. Für eine zusätzliche Serie gehen sie genau gleich vor. 
   **Hinweis**: Jede Serie enthält die gleichen Kategorien. Die Kategorien müssen sie daher nur einmal definieren



![Diagrammerstellen_7](images/Diagrammerstellen_7.png)

9. Wenn sie das Diagramm in Excel markiert haben, finden sie unter dem Menupunkt "Chart Design" den Knopf "Add Chart Element". Hier können sie Titel, Achsenbeschriftungen, Legenden und weiteres hinzufügen und einstellen



## Zusätzliche Hinweise

Excel versucht automatisch die Daten korrekt darzustellen. 

**Beispiel mit Säulendiagramm und einer Serie**: Die Serienbezeichnung (Punkt 6 oben) wird automatisch als Diagrammtitel übernommen. Wenn sie die Legend einblenden wird dort der gleiche Text stehen.

**Beispiel mit Säulendiagramm und zwei Serien**: Die Serienbezeichnungen (nun in Mehrzahl) können nicht mehr als Diagrammtitel walten. Daher wird der Titel weggelassen und die Bezeichnungen werden in der Legende dargestellt. Beide Elemente (Titel und Legende) sind aber per Standard ausgeblendet.

Natürlich haben die verschiedenen Diagrammtypen noch zusätzliche Einschränkungen, die jeweils zu beachten sind. Ein Kuchendiagramm kann z.B. nur eine Serie haben. Bei mehreren Serien werden pro Serie ein Kuchendiagramm erstellt.