# Auftrag 05

Gegeben sind 5 Tabellen im [PDF-Dokument](./Tabellen_Rechnungsverwaltung.pdf). 

- Analysieren Sie die Tabellen 

- Erstellen Sie in [draw.io](draw.io) ein logisches Datenmodell in chen-Notation

  
  
  **Hinweis:** Zur Hilfe können Sie das [cheat-sheet](../../tag06/ERD-Beziehungen/cheatsheet_beziehungstypen.docx) verwenden
  
  

**Zeit**: 45 Minuten

**Form**: in 2er Gruppen

**Tools**: mySQL-workbench, draw.io

