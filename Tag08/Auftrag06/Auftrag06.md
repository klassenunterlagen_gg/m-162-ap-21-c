# Auftrag 06

Gegeben ist die Tabelle Rechnungsverwaltung. 

- Analysieren Sie die Tabelle

- Gehen Sie schrittweise vor und Normalisieren Sie die Tabelle. Für jeden Normalfall erstellen Sie wiederum eine eigene Tabelle(n). 

- Am Ende erstellen Sie in [draw.io](draw.io) ein logisches Datenmodell in chen-Notation.

  

**Zeit**: 90 Minuten

**Form**: in 2er Gruppen

**Tools**: Excel, draw.io

