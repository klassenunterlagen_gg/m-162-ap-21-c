# Open Data

**Präsentation von Daten**

https://www.youtube.com/watch?v=jbkSRLYSojo

**Beispiel-Applikationen**
 * [http://radar.zhaw.ch Flugradar]
 * [http://maps.vasile.ch/transit-sbb/ Zugradar]
 * [http://www.marinetraffic.com/de/ Schiffsradar] und [https://www.shipmap.org/ Schiffsbewegungen]
 * [http://earth.nullschool.net/about.html Wind und Strömungen auf der Erde]
 * [http://metrocosm.com/global-migration-map.html Migrationsströme]
 * [https://www.youtube.com/watch?v=1XBwjQsOEeg 24 Stunden Flugbewegungen in einer Minute]
 * [https://map.blitzortung.org/#8.03/42.275/12.29 Blitzortung auf openmap]

**Bewegung im Internet**

* [http://opendatahandbook.org/ Open Data Handbook]
* [http://www.bfs.admin.ch/ Bundesamt für Statistik]
* [http://opendata.ch/ opendata.ch]
* [http://opendata.swiss opendata.swiss OGD - open government data]
* [https://www.stadt-zuerich.ch/portal/de/index/ogd.html Stadt Zürich - open government data]

**politische/ökonomische Bewegungen**

* [https://de.wikipedia.org/wiki/Das_Kapital_im_21._Jahrhundert Das Kapital im 21. Jahrhundert] von Thomas Piketty
* [http://www.parisschoolofeconomics.eu Paris School of Economics]
* [https://www.credit-suisse.com/corporate/de/articles/news-and-expertise/global-wealth-report-2018-us-and-china-in-the-lead-201810.html Crédit Suisse - Global Wealth Report]

**Beispiele für Datenquellen**

* [https://data.stadt-zuerich.ch/dataset/vbz_fahrgastzahlen_ogd Fahrgastzahlen der VBZ]
* [http://topincomes.parisschoolofeconomics.eu/ Datenbasis der Paris School of Economics (Piketty)]
* [https://data.nasa.gov/ Nasa]
* [https://earthdata.nasa.gov/ Nasa Earthdata]
* [http://ec.europa.eu/eurostat/data/database Eurostat]
* [https://datahub.io/ datahub]
* [http://opendata.cern.ch/ CERN Genf]
* [https://www.flughafen-zuerich.ch/unternehmen/laerm-politik-und-umwelt/flugbewegungen/bewegungsstatistik Bewegungsstatistik Flughafen Zürich]
* [https://sensors-eawag.ch/sarscov2/ARA_Werdhoelzli_ddPCR.html EAWAG - Abwassersensoren]
* [https://portal.endress.com/irj/portal?j_username=e00100196&j_password=123456&NavigationTarget=navurl://d3fa4e32b76b0a218ed731a147a91d2a&NavMode=3 Eichener See] - Pegelstände [https://www.verlagshaus-jaumann.de/inhalt.maulburg-messgeschichte-des-sagenumwobenen-sees.667a4418-1e34-47a2-8de9-dd1f4c9f2dea.html Berichte]

**vielleicht nicht ganz open data, aber ganz interessant**
* [https://www.intervista.ch/mobilitaets-monitoring-covid-19/ Bewegungsdaten während Covid-19]
* [https://trends.google.ch/trends/?geo=CH Google - Trends]
* [https://www.bing.com/toolbox/keywords Microsoft Bing Infos]

**Datenanonymisierung**

Open Data kann zu Widersprüchen bezüglich Datenschutz und Persönlichkeitsrechten stehen. Bei dem Bestreben personenbezogene Daten zu anonymisieren, muss aber sehr sorgfältig vorgegangen werden.  AOL hat im Jahre 2006 einen sehr naiven Vorstoss gemacht und von ca. 600.000 Usern die Suchanfragen "anonymisiert" veröffentlicht. Nach kurzer Zeit wurden die Daten zwar von der Website wieder entfernt, aber die Daten waren natürlich schon verbreitet und sind bis heute verfügbar.
* [https://www.heise.de/newsticker/meldung/AOL-veroeffentlichte-Suchanfragen-von-ueber-500-000-Mitgliedern-149433.html Heise online AOL veröffentlicht Suchanfragen]
* [https://netzpolitik.org/2006/aol-veroeffentlicht-suchanfragen/ Netzpolitik.org
