# Auftrag 04

In Auftrag 01 und 02 haben Sie ein konzeptuelles Datenmodell erstellt. Erstellen Sie nun ein logisches Datenmodell in mySQL workbench.

- Erstellen Sie ein neues workbench-Modell
- Erstellen Sie Tabellen
- Legen Sie die Datentypen für die Attributen fest
- Fügen Sie ein Diagramm hinzu


*Frage*

- Welche Erweiterung in einer der Tabellen müssen Sie vornehmen, damit eine Beziehung funktioniert?

**Zeit**: 15 Minuten

**Form**: in 2er Gruppen

**Tools**: mySQL-workbench, draw.io

