# Datenstrukturtypen und Dateitypen

**Autoren**: Andy Corsten, Yves Nussle
**Datum**: 16.08.2021

Die folgenden Datenstrukturen sind in der Regel für die klassische imperative Programmierung entwickelt und optimiert worden. Andere Programmierparadigmen wie die funktionale Programmierung können durchaus andere Datenstrukturen erfordern.

## Text

Text ist eine „Anhäufung“ von zusammengehörenden Zeichen (Alphabet) die einen Sachverhalt beschreiben. Diese Form ist besonders gut geeignet für Volltextsuche. Text-Strukturen sind im Sinne der Strukturierung sehr schwach strukturiert. Da eine Darstellung in Textdateien bei einer Suche nicht hilft. Allerdings sind diese „Strukturen“ ganz leicht zu bearbeiten.

**Typische Dateierweiterungen**: TXT, JSON; XML, CSV, HTML

## Grafik

Eine grafische Darstellung ist „nur“ eine Momentaufnahme. Sie besitzt gar keine Struktur. Von der elektronischen Speicherung gibt es zwei grundlegend verschiedene Möglichkeiten: 

- die Punktförmige Speicherung (Pixelgrafik) oder 
- die berechnete Darstellung (Vektorgrafik). Hier wird jeder Punkt berechnet.

Die Grafik wird als gar nicht strukturierte Daten betrachtet. Unterschiede bei den Bildformaten: https://www.labnol.org/software/tutorials/jpeg-vs-png-image-quality-or-bandwidth/5385/

**Typische Dateierweiterungen**: PNG, JPG, GIF, SVG

## Video / Sound

Sowohl Video und Sound sind analoge Signale, die digitalisiert werden. Auch hier sprechen wir von KEINER Struktur.

**Typische Dateierweiterungen**: WAV, MP3, MP4, AVI, MPEG, MKV

## Tabellen

Eine Tabelle ist eine geordnete Zusammenstellung von Texten oder Daten. Die darzustellenden Inhalte werden dabei in Zeilen (waagerecht) und Spalten (senkrecht) gegliedert, die aneinander ausgerichtet werden. Die erste Spalte wird oft Vorspalte oder Zeilen Nummerierung genannt, die erste Zeile nennt man oft Kopfzeile oder Spaltenbeschriftung. Weiterhin bestehen Tabellen aus Tabellenfeldern (Zellen). Der Verweis auf ein Feld ist die Adressierung (Referenz)

Meist besteht ein semantischer Zusammenhang zwischen dem Inhalt einer Zelle und Zeile bzw. Spalte, in der er sich befindet. Wir verstehen unter dem Begriff Tabelle mehrere Bedeutungen:

1. Eine Tabellenkalkulation ist ein Computerprogramm, das vor allem zur Darstellung und Berechnung auch hypothetischer Zahlendaten in großem Umfang geeignet ist.
2. Bei relationalen Datenbanken steht der Ausdruck Tabelle für eine Sammlung ähnlich strukturierter Daten, die technisch meist zusammengehörig gespeichert sind. Sie hat je nach System unterschiedliche Bezeichnungen: Tabelle, Entität, Relation
3. In der Aussagenlogik existieren Wahrheitstabellen, die verschiedene Operationen und ihre möglichen Ergebnisse übersichtlich darstellen.

Wenn wir von Tabellen reden, meinen wir meistens Tabellenkalkulationen. 

Tabellen sind mässig strukturiert. Ihre Struktur ist für einen Fokus generiert und kann anschliessend nur mit viel Aufwand verändert werden. Innerhalb dieses Fokus ist die Tabelle sehr gut geeignet um zu sortieren und Auszuwerten.

## Datensatz / Record / Tupel

Datensätze (auch 'Tupel' oder engl. Record genannt) gehören zu den einfachsten Datenstrukturen. Sie verkörpern Werte, die andere Werte enthalten, üblicherweise in einer fest definierten Anzahl und Folge. Datensätze identifizieren sich meist durch eines oder mehrere der in ihnen enthaltenen Elemente, oft Datenfeld genannt.

**Beispiele**: 

- (ID=12, Vorname=Hans, Nachname=Wenger, Telefonnummer=07911111111, Adresse=Zuercherstrasse 12)
- [Länge=4.5, Song=Patience, Band=Guns'n'Roses]

Ob die Klammern rund oder eckig sind, spiel hier keine Rolle. Wichtig ist, dass die Daten thematisch zusammen gehören und normalerweise mehrere Tuples des gleichen Typs existieren. Ein Tuple/Record kann als eine Zeile einer Tabelle angeschaut werden.

## Array

Das Array (auch Feld) ist die einfachste verwendete Datenstruktur. Es werden hierbei mehrere Variablen vom selben Basisdatentyp gespeichert. Ein Zugriff auf die einzelnen Elemente wird über einen Index möglich. Technisch gesehen entspricht dieser dem Wert, der zu der Startadresse des Arrays im Speicher addiert wird, um die Adresse des Objektes zu erhalten. Die einzigen notwendigen Operationen sind das indizierte Speichern und das indizierte Lesen, die auf jedes Element des Arrays direkt zugreifen können. Im eindimensionalen Fall wird das Array häufig als Vektor und im zweidimensionalen Fall als Tabelle oder Matrix bezeichnet. Arrays sind aber keinesfalls nur auf zwei Dimensionen beschränkt, sondern werden beliebig mehrdimensional verwendet. Wegen ihrer Einfachheit und grundlegenden Bedeutung bieten die allermeisten Programmiersprachen eine konkrete Umsetzung dieser Datenstruktur als zusammengesetzten Datentyp Array im Grundsprachumfang an.

Einen Sonderfall bildet das assoziative Array (oft auch Dictionary genannt), bei dem nicht (nur) über einen numerischen Index, sondern über einen Schlüssel auf die gespeicherten Daten zugegriffen wird. Eine mögliche Art, ein assoziatives Array zu implementieren, ist die Hashtabelle.

**Beispiele**:

- [12, 45, 23, 38, 28]
- ["Hans", "Werner", "Sabine", "Rafael", "Susanne"]
- ["pos01" = "Position 01", "pos02" = "Position 02", ...] => Hier handelt es sich um ein assoziatives Array und darum muss der Schlüssel mit angegeben werden. Also an der Position "pos01" befindet sich der Wert "Position 01"

## Verkettete Liste

Die verkettete Liste ist eine Datenstruktur zur dynamischen Speicherung von beliebig vielen Objekten. Dabei beinhaltet jedes Listenelement einer verketteten Liste als Besonderheit einen Verweis auf das nächste Element, wodurch die Gesamtheit der Objekte zu einer Verkettung von Objekten wird. Die zu einer Liste gehörenden Operationen sind relativ unspezifiziert. Sie werden oft in komplizierteren Datenstrukturen verwendet und statt über Operationen wird dort aus Effizienzgründen meist direkt auf ihre Elemente zugegriffen. Die in Programmbibliotheken angebotenen Listen sind in ihrer zugrunde liegenden Datenstruktur meist viel komplexer und stellen nicht selten gar keine echten Listen dar, geben sich nach außen aber als solche aus. Listen sind stets lineare Strukturen.

**Beispiel**: http://www.codeadventurer.de/?p=1844

<img src="images/verkettete_liste_knoten.jpg" alt="verkettete_liste_knoten" />

## Stapelspeicher

In einem Stapelspeicher (engl. stack oder ‚Kellerspeicher‘) kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte nur in umgekehrter Reihenfolge wieder gelesen werden. Dies entspricht dem LIFO-Prinzip. Für die Definition und damit die Spezifikation des Stapelspeichers ist es unerheblich, welche Objekte in ihm gespeichert werden. Zu einem Stapelspeicher gehören zumindest die Operationen

- push, um ein Objekt im Stapelspeicher abzulegen und
- pop, um das zuletzt gespeicherte Objekt wieder zu lesen und vom Stapel zu entfernen.

Weitere Optionen können top oder peek sein, um das oberste Element zu lesen, ohne es zu löschen.

Die Top-Operation ist nicht zwingend vorgeschrieben, wird aber oft implementiert, um pop/push zu ersetzen, da es oft interessant ist, das oberste Element zu „testen“. Ein Stapelspeicher wird gewöhnlich als Liste implementiert, kann aber auch ein Vektor sein.

**Beispiel**: https://de.wikipedia.org/wiki/Stapelspeicher

<img src="images/stapelspeicher.png" alt="stapelspeicher" />

## Warteschlange

In einer Warteschlange (engl. queue) kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte nur in der gleichen Reihenfolge wieder gelesen werden, wie sie gespeichert wurden. Dies entspricht dem FIFO-Prinzip. Für die Definition und damit die Spezifikation der Queue ist es unerheblich, welche Objekte in ihm gespeichert werden. Zu einer Queue gehören zumindest die Operationen

- enqueue, um ein Objekt in der Warteschlange zu speichern und
- dequeue, um das zuerst gespeicherte Objekt wieder zu lesen und aus der Warteschlange zu entfernen.

Eine Warteschlange wird gewöhnlich als verkettete Liste implementiert, kann intern aber auch ein Array verwenden; in diesem Fall ist die Anzahl der Elemente begrenzt.

Beispiel: https://de.wikipedia.org/wiki/Warteschlange_(Datenstruktur)

<img src="images/queue.png" alt="queue" />

## Vorrangwarteschlange

Eine Spezialisierung der Warteschlange ist die Vorrangwarteschlange, die auch Prioritätswarteschlange bzw. engl. Priority Queue genannt wird. Dabei wird vom FIFO-Prinzip abgewichen. Die Durchführung der enqueue-Operation, die in diesem Fall auch insert-Operation genannt wird, sortiert das Objekt gemäß einer gegebenen Priorität, die jedes Objekt mit sich führt, in die Vorrangwarteschlange ein. Die dequeue-Operation liefert immer das Objekt mit der höchsten Priorität. Vorrangwarteschlangen werden meist mit Heaps implementiert.

**Beispiel**: https://de.wikipedia.org/wiki/Vorrangwarteschlange

<img src="images/priority.png" alt="priority" />

## Graph

Ein Graph ermöglicht es als Datenstruktur die Unidirektionalität der Verknüpfung zu überwinden. Die Operationen sind auch hier das Einfügen, Löschen und Finden eines Objekts. Die bekannteste Repräsentation von Graphen im Computer sind die Adjazenzmatrix, die Inzidenzmatrix und Adjazenzliste.

**Beispiel:** https://de.wikipedia.org/wiki/Graph_(Graphentheorie)

<img src="images/graph.png" alt="graph" />

## Baum

Bäume sind spezielle Formen von Graphen in der Graphentheorie. Als Datenstruktur werden meist nur Out-Trees verwendet. Dabei können ausgehend von der Wurzel mehrere gleichartige Objekte miteinander verkettet werden, sodass die lineare Struktur der Liste aufgebrochen wird und eine Verzweigung stattfindet. Da Bäume zu den meist verwendeten Datenstrukturen in der Informatik gehören, gibt es viele Spezialisierungen.

So beträgt bei Binärbäumen die Anzahl der Kinder höchstens zwei und in höhen-balancierten Bäumen gilt zusätzlich, dass sich die Höhen des linken und rechten Teilbaums an jedem Knoten nicht zu sehr unterscheiden.

Bei geordneten Bäumen, insbesondere Suchbäumen, sind die Elemente in der Baumstruktur geordnet abgelegt, sodass man schnell Elemente im Baum finden kann. Man unterscheidet hier weiter in binäre Suchbäume mit AVL-Bäumen als balancierte Version und B-Bäumen sowie einer Variante, den B*-Bäumen. Spezialisierungen von B-Bäumen sind wiederum 2-3-4-Bäume, welche oft als Rot-Schwarz-Bäume implementiert werden.

Nicht sortiert, aber „verschachtelt“ sind geometrische Baumstrukturen wie der R-Baum und seine Varianten. Hier werden nur diejenigen Teilbäume durchsucht, die sich mit dem angefragten Bereich überlappen.

Bäume sind in ihrem Aufbau zwar mehrdimensional jedoch in der Verkettung der Objekte oft unidirektional. Die Verkettung der gespeicherten Objekte beginnt bei der Wurzel des Baums und von dort in Richtung der Knoten des Baums.

**Beispiel**: https://de.wikipedia.org/wiki/Baum_(Graphentheorie)

<img src="images/tree.png" alt="tree" />

## Heap

Der Heap (auch Halde oder Haufen) vereint die Datenstruktur eines Baums mit den Operationen einer Vorrangwarteschlange. Häufig hat der Heap neben den minimal nötigen Operationen wie insert, remove und extractMin auch noch weitere Operationen wie merge oder changeKey. Je nach Reihenfolge der Priorität in der Vorrangwarteschlange wird ein Min-Heap oder ein Max-Heap verwendet. Weitere Spezialisierungen des Heap sind der Binäre Heap, der Binomial-Heap und der Fibonacci-Heap. Heaps werden meistens über Bäume aufgebaut.

**Beispiel**: https://de.wikipedia.org/wiki/Heap_(Datenstruktur)

<img src="images/heap.png" alt="heap" />

## Hashtabelle

Die Hashtabelle bzw. Streuwerttabelle ist eine spezielle Indexstruktur, bei der die Speicherposition direkt berechnet werden kann. Hashtabellen stehen dabei in Konkurrenz zu Baumstrukturen, die im Gegensatz zu Hashtabellen alle Indexwerte in einer Ordnung wiedergeben können, aber einen größeren Verwaltungsaufwand benötigen, um den Index bereitzustellen. Beim Einsatz einer Hashtabelle zur Suche in Datenmengen spricht man auch vom Hashverfahren. Bei sehr großen Datenmengen kann eine verteilte Hashtabelle zum Einsatz kommen.

**Beispiel**: https://de.wikipedia.org/wiki/Hashtabelle

<img src="images/hash.png" alt="hash" />
