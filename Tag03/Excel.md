# Excel

**Formeln**

https://support.microsoft.com/de-de/office/%C3%BCbersicht-%C3%BCber-formeln-in-excel-ecfdc708-9162-49e8-b993-c311f47ca173

**Text Import**

https://support.microsoft.com/en-us/office/text-import-wizard-c5b02af6-fda1-4440-899f-f78bafe41857#:~:text=Go%20to%20the%20Data%20tab,Import%20Wizard%20dialog%20will%20open.

**Filtern**

https://support.microsoft.com/de-de/office/filtern-von-daten-in-einem-bereich-oder-in-einer-tabelle-01832226-31b5-4568-8806-38c37dcc180e

**Sortieren**

https://support.microsoft.com/de-de/office/sortieren-von-daten-in-einem-bereich-oder-in-einer-tabelle-62d0b95d-2a90-4610-a6ae-2e545c4a4654

**Formeln verschieben / kopieren**

https://support.microsoft.com/de-de/office/verschieben-oder-kopieren-einer-formel-1f5cf825-9b07-41b1-8719-bf88b07450c6#:~:text=W%C3%A4hlen%20Sie%20die%20Zelle%20aus,dr%C3%BCcken%20Sie%20STRG%2BV).
