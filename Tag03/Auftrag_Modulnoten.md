# Auftrag: Modulnoten

In dieser Übung werden sie mit Excel arbeiten, ähnlich wie sie es im Unterricht gesehen haben. Als Grundlage dient ein Beispiel mit Noten für Module.

Ob sie die das Semester wiederholen müssen, hängt von folgenden **fiktiven** Kriterien ab:

- Sie dürfen nicht mehr als 1 ungenügende Modulnote haben
- Die Anzahl Minuspunkte darf nicht höher als 1 sein. Ein Minuspunkt ist die Differenz zu der Note 4. Eine Modulnote von einer 3.5 wäre also 0.5 Minuspunkte.
- Der Schnitt aller Modulnoten muss mindestens 4 sein.

#### Aufgaben

- Laden sie die [TXT-Datei](Tag03/Noten.txt) herunter. 
- Importieren sie die Daten in Excel. Ob sie zuvor in CSV umwandeln ist fakultativ
- Berechnen sie die notwendigen Felder. Das folgende Bild zeigt wie das Ergebnis aussehen soll, wobei die farblich hervorgehobenen Felder berechnet werden müssen.
- Notwendige Formeln: average, mround, countif, sumif, if, and

<img src="noten-struktur.png" alt="noten-struktur" style="zoom:60%;" />

#### Zeit und Form

- 30 Minuten
- Individuell

