# Auftrag: JSON Steckbrief

In dieser Übung geht es darum, dass sie JSON Daten erstellen können. Dazu verwenden sie die Inputs zu den Steckbriefen vom ersten Tag.

#### Aufgaben

- Nehmen sie die Erkenntnisse des ersten Tages und bringen sie mögliche Steckbrief-Daten in ein JSON-Format. 
- Stellen sie sicher, dass sie alle Typen von JSON in ihrer Struktur verwenden (Listen, Objekte, Datentypen).
- Fügen sie so ca 4-5 Datensätze ein.
- Validieren sie ihre Daten mit dem Online-Tool

#### Zeit und Form

- 30 Minuten
- Individuell

