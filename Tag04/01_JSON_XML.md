# JSON versus XML

## JSON

#### Tutorials

[JSON](https://www.youtube.com/watch?v=unXWk2plEoc) 5 Minuten und deutsch von lerneprogrammieren

[W3Schools JSON](https://www.w3schools.com/js/js_json_intro.asp)

https://www.youtube.com/watch?v=iiADhChRriM&ab_channel=WebDevSimplified (12 Minuten)

https://www.tutorialspoint.com/json/index.htm

https://www.json.org/json-en.html (sehr formal)

#### Tools

https://jsonformatter.curiousconcept.com/
Formatiert und validiert JSON

## XML

[XML](https://www.youtube.com/watch?v=RkLu9TO2V2U) 5 Minuten und deutsch von lerneprogrammieren

[W3Schools XML](https://www.w3schools.com/xml/)

## Vergleich JSON und XML

kommt noch :-)
