# Auftrag: JSON Adressdaten interpretieren

In dieser Übung geht es darum, dass sie JSON Daten interpretieren können. 

#### Aufgaben

- Generieren sie Adressdaten mit den Online-Tool: https://migano.de/testdaten.php. Stellen sie sicher, dass sie verschiedene Basis-Datentypen generieren (Datum, String, Zahlen, etc)
- Konvertieren sie die Daten nach JSON mit dem Online-Tool: http://www.convertcsv.com/csv-to-json.htm
- Versuchen sie die Daten und Syntax zu interpretieren. Schreiben sie dazu ein paar wenige Sätze. Vergleichen sie die Syntax mit den Ursprungsdaten.
- Gibt es offene Fragen, die JSON in diesem Beispiel nicht abdeckt?

#### Zeit und Form

- 20 Minuten
- Individuell

